# Action prediction from ongoing videos

_Disclaimer: this bachelor’s thesis came to be as part of the Computer Sciences course at the University Hasselt, under supervision of promotor Prof. Philippe Bekaert and counselors Mr Lode Jorissen, Dr Nick Michiels, and Dr Jeroen Put._

This thesis is a re-implementation (with minor changes) of the paper "Human activity prediction: Early recognition of ongoing activities from streaming videos" by M.S. Ryoo<sup>[1](#cite_1)</sup>. It is written in Rust and uses OpenCV2. The feature descriptor is the "STIP descriptor" binary provided by Laptev<sup>[2](#cite_2)</sup>.

<a name="cite_1">1</a>: M. S. Ryoo. "Human activity prediction: Early recognition of ongoing activities from streaming videos". In: 2011 International Conference on Computer Vision (Nov. 2011). doi: 10.1109/iccv.2011.6126349.

<a name="cite_2">2</a>: Ivan Laptev. Download data/software. url: https://www.di.ens.fr/~laptev/download.html, archived at https://web.archive.org/web/20130222032238/https://www.di.ens.fr/~laptev/download.html on Feb. 22, 2013.
