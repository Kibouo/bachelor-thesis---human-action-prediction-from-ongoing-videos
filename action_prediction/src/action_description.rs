use crate::{matrix::mat::Mat, stip::Stip};

// TODO: play with the params of kmeans. Higher numbers take longer to calculate
// but are more precise. If the amount of attempts or iterations is too low, the
// clustering will not be good enough. This causes incorrect predictions in some
// runs!
//
// This is currently our biggest bottleneck!
const BOW_KMEANS_ATTEMPTS: usize = 1;
const BOW_KMEANS_ITERATIONS: usize = 1000;
const BOW_KMEANS_EPSILON: f64 = 0.000_000_000_000_000_000_01;

/// A struct storing all raw data of, and thus describing, a specific action.
#[derive(Clone, Default, PartialEq)]
pub struct ActionDescription
{
    pub name:     String,
    pub features: Vec<Stip>
}

impl ActionDescription
{
    /// Calculates the cluster centers of the action's features using k-means.
    pub fn calc_clusters(&self) -> Mat
    {
        // Our implementation only cares about the HOG descriptors of the features.
        //
        // NOTE: due to our implementation summing all HOGs into 1, we only care about
        // idx 0.
        let hogs = self
            .features
            .iter()
            .filter_map(|i| i.hog_descr().as_ref())
            .filter_map(|i| i.get(0));
        let hogs_per_row = hogs
            .map(Mat::from)
            .fold(Mat::default(), |mut hogs_per_row, hog| {
                hogs_per_row.push_back(&hog).unwrap();
                hogs_per_row
            });

        use crate::hist::feature_hist::AMT_VISUAL_WORDS;
        use opencv::{core::{TermCriteria,
                            TermCriteria_COUNT,
                            TermCriteria_EPS,
                            KMEANS_PP_CENTERS},
                     features2d::BOWKMeansTrainer};

        let kmeans_clusterer = BOWKMeansTrainer::new_with_criteria(
            AMT_VISUAL_WORDS as i32,
            &TermCriteria::new(
                TermCriteria_EPS + TermCriteria_COUNT,
                BOW_KMEANS_ITERATIONS as i32,
                BOW_KMEANS_EPSILON
            )
            .unwrap(),
            BOW_KMEANS_ATTEMPTS as i32,
            KMEANS_PP_CENTERS
        )
        .expect("Failed to create BOWKMeansTrainer.");

        Mat::from(
            kmeans_clusterer
                .new(&hogs_per_row)
                .expect("Clustering failed.")
        )
    }
}
