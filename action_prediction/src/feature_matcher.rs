use crate::{matrix::mat::Mat, stip::Stip};
use opencv::{features2d::BFMatcher, types::VectorOfMat};

/// A wrapper for OpenCV's ``BFMatcher`` type. This allows us to implement
/// traits, as well as detach our code from a specific OpenCV wrapper/library.
pub struct FeatureMatcher(BFMatcher);

impl FeatureMatcher
{
    /// Labels, i.e. finds the corresponding word/bucket, for a vector of
    /// STIPs. Returns a mapping to indices where each index indicates the
    /// closest center/bucket/word of the mapped STIP.
    pub fn label_features(
        &mut self,
        stips: &[Stip]
    ) -> Vec<usize>
    {
        use opencv::{features2d::DescriptorMatcher, types::VectorOfDMatch};

        let masks = VectorOfMat::new();
        let matches = VectorOfDMatch::new();
        let mut stips_in_mat = Mat::default();
        for stip in stips.iter()
        {
            stips_in_mat
                .push_back(&Mat::from(
                    stip.hog_descr()
                        .as_ref()
                        .expect("STIP has no HOG.")
                        .get(0)
                        .expect("STIP has no HOG.")
                ))
                .expect("Failed to push STIP onto collecting Mat.");
        }

        self.matches(&stips_in_mat, &matches, &masks)
            .expect("Error during matching.");

        matches.iter().map(|m| m.trainIdx as usize).collect()
    }
}

/// Creates a new matcher object given some training data (cluster centers).
impl From<Mat> for FeatureMatcher
{
    fn from(centers: Mat) -> Self
    {
        use opencv::{core::NORM_L2, features2d::DescriptorMatcher};
        use crate::matrix::IntoInner;

        let mut clust_centers = VectorOfMat::new();
        clust_centers.push(centers.into_inner());

        let mut matcher = BFMatcher::new(NORM_L2, false).expect("Failed to create BFMatcher.");
        matcher
            .add(&clust_centers)
            .expect("Unable to add centers to matcher.");

        FeatureMatcher(matcher)
    }
}

impl std::ops::Deref for FeatureMatcher
{
    type Target = BFMatcher;

    fn deref(&self) -> &Self::Target { &self.0 }
}

impl std::ops::DerefMut for FeatureMatcher
{
    fn deref_mut(&mut self) -> &mut BFMatcher { &mut self.0 }
}
