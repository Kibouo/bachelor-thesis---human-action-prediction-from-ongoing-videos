use crate::{action_description::ActionDescription, hist::integral_hist::IntegralHist, stip::Stip};
use std::{collections::HashMap,
          fs::File,
          io::{BufRead, BufReader}};

pub fn parse_input(reader: impl BufRead) -> Vec<ActionDescription>
{
    fn push_action(actions: &mut Vec<ActionDescription>, mut action: ActionDescription){
        // Sort features based on the frame they appear in, in chronological order.
        action.features.sort_unstable_by(|f1, f2| f1.t().cmp(f2.t()));
        actions.push(action);
    }

    let mut actions = Vec::new();
    let mut action = ActionDescription::default();

    for line in reader.lines().skip(1).map(Result::unwrap) // Skip 1st line which describes feature layout
    {        
        match line.get(0..1)
        {
            Some("#") =>
            {
                action.name = line.get(2..).unwrap().to_string();
            }
            Some(_) =>
            {
                action.features.push(line.parse::<Stip>().unwrap());
            }
            None =>
            {
                push_action(&mut actions, action);
                action = ActionDescription::default();
            }
        }
    }
    // the above None option triggers on newlines EXCEPT the very last newline of a
    // file (reader.lines() probably just skips this very last one without
    // mentioning it in the docs). Thus, we need to add the last constructed action
    // here. However, this causes an empty ActionDescriptor to be added if there
    // were no lines at all to start with. To prevent this we do following equality
    // check.
    if action != ActionDescription::default()
    {
        push_action(&mut actions, action);
    }

    actions
}

/// Group variations of actions together by main action name.
fn group_actions(
    varying_actions: impl IntoIterator<Item = ActionDescription>
) -> HashMap<String, Vec<ActionDescription>>
{
    varying_actions.into_iter().fold(
        HashMap::new(),
        |mut action_bins: HashMap<String, Vec<ActionDescription>>, action| {
            let core_name = main_action_name(&action.name);
            action_bins.entry(core_name).or_default().push(action);
            action_bins
        }
    )
}

/// Clustering of the feaBufReader::newtures will be done using k-means. The complexity of
/// this algorithm is ``OBufReader::new(t*k*n*d)`` with ``t`` the amount of iterations, ``k``
/// the number of desiredBufReader::new clusters, ``n`` the amount of samples to cluster, and
/// ``d`` the dimension oBufReader::newf the samples.
///BufReader::new
/// Thus, features (i.e. BufReader::newthe samples) consisting of multiple HOGs will slow down
/// the k-means algorithmBufReader::new due to their big dimensionality. To optimise we thus
/// add up all HOGs of a BufReader::newfeature.
///BufReader::new
/// The feature descriptoBufReader::newrs used in the paper consist of the sum of all
/// gradients. We use HOGBufReader::news with 4 buckets, but summing is still appropriate.
pub fn sum_hogs_per_stip(
    actions: impl IntoIterator<Item = ActionDescription>
) -> impl Iterator<Item = ActionDescription>
{
    actions.into_iter().map(|mut action| {
        action.features = action
            .features
            .into_iter()
            .map(|mut stip| {
                stip.sum_hogs();
                stip
            })
            .collect();
        action
    })
}

/// Calculate for each class of action an average integral histogram over all
/// variations of that action.
pub fn train_action_models(filename: &str) -> Vec<IntegralHist>
{
    let training_data = parse_input(BufReader::new(File::open(filename).unwrap()));
    group_actions(sum_hogs_per_stip(training_data))
        .into_iter()
        .map(IntegralHist::from)
        .collect::<Vec<IntegralHist>>()
}

/// Creates an ActionDescriptor per observed second.
pub fn split_action_per_second(full_action: ActionDescription) -> Vec<ActionDescription>
{
    fn named_action(name: String) -> ActionDescription {
        let mut action = ActionDescription::default();
        action.name = name;
        action
    }

    use crate::prediction_fn::FIXED_OBSERV_DURATION;

    let ActionDescription { name, features } = full_action;
    let total_secs = (features.iter().map(|feature| feature.t()).max().unwrap() / FIXED_OBSERV_DURATION as usize) + 1;
    
    features.into_iter().fold((0..total_secs).map(|_| named_action(name.clone())).collect::<Vec<ActionDescription>>(), |mut actions_per_second, feature| {
        let second = feature.t() / FIXED_OBSERV_DURATION as usize;

        let action = actions_per_second.get_mut(second).unwrap_or_else(|| panic!("Second {} index not found for total amt seconds {} and action '{}'", second, total_secs, name.clone()));
        action.features.push(feature);

        actions_per_second
    })
}

// Parses an action's name and returns the main action's name.
//
// We expect training data to have name annotations in the form of
// 'main-sub[...]' where main consists of any ASCII symbol EXCEPT '-'.
fn main_action_name(s: &str) -> String { s.split('-').next().unwrap().to_string() }
