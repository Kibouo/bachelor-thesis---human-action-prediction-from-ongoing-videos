use crate::matrix::{covar_mat::CovarMat, mat::Mat};

// TODO: play with this. Higher takes longer to calculate but is more precise.
pub const AMT_VISUAL_WORDS: usize = 100;

/// A feature histogram for the features up to time point ``d``.
///
/// This is a histogram where each bucket is a visual word (i.e. feature
/// of the same 'type', ignoring the spatio-temporal location), and the height
/// of a bar is determined by the amount of features that correspond to
/// that visual word. The amount of features is taken over the time-interval
/// ``[0..d]``, where ``d`` is the current frame.
#[derive(Clone)]
pub struct FeatureHist
{
    pub buckets: [f32; AMT_VISUAL_WORDS],
    covar:       Option<CovarMat>
}

impl FeatureHist
{
    pub fn covar(&self) -> &Option<CovarMat> { &self.covar }
}

impl Default for FeatureHist
{
    fn default() -> FeatureHist
    {
        FeatureHist {
            buckets: [0.0; AMT_VISUAL_WORDS],
            covar:   None
        }
    }
}

impl From<Mat> for FeatureHist
{
    fn from(samples: Mat) -> FeatureHist
    {
        use crate::matrix::Indexing;
        
        let (covar, mut mean) = CovarMat::calc_auto_covar_matrix(&samples, None);

        let mut res = FeatureHist::default();
        for i in 0..AMT_VISUAL_WORDS
        {
            res.buckets[i] = *mean.at_2d(0, i);
        }
        res.covar = Some(covar);

        res
    }
}

impl From<[f32; AMT_VISUAL_WORDS]> for FeatureHist
{
    fn from(buckets: [f32; AMT_VISUAL_WORDS]) -> FeatureHist
    {
        let mut res = FeatureHist::default();

        res.buckets = buckets;
        res.covar = None;

        res
    }
}

impl std::ops::Sub<&FeatureHist> for &FeatureHist
{
    type Output = FeatureHist;

    fn sub(
        self,
        other: &FeatureHist
    ) -> Self::Output
    {
        let mut res = FeatureHist::default();
        for i in 0..AMT_VISUAL_WORDS
        {
            res.buckets[i] = self.buckets[i] - other.buckets[i];
        }
        res
    }
}

impl std::ops::Sub<f32> for &FeatureHist
{
    type Output = FeatureHist;

    fn sub(
        self,
        rhs: f32
    ) -> Self::Output
    {
        let mut res = FeatureHist::default();
        for i in 0..AMT_VISUAL_WORDS
        {
            res.buckets[i] = self.buckets[i] - rhs;
        }
        res
    }
}

impl std::ops::Add<f32> for &FeatureHist
{
    type Output = FeatureHist;

    fn add(
        self,
        rhs: f32
    ) -> Self::Output
    {
        let mut res = FeatureHist::default();
        for i in 0..AMT_VISUAL_WORDS
        {
            res.buckets[i] = self.buckets[i] + rhs;
        }
        res
    }
}
