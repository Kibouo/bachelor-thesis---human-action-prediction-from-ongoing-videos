use crate::{action_description::ActionDescription,
            feature_matcher::FeatureMatcher,
            hist::feature_hist::FeatureHist,
            matrix::mat::Mat,
            stip::Stip};

/// An advanced description of an action.
///
/// An integral histogram of visual words is a chronological sequence of feature
/// histograms. It can be looked at as a function which describes how the amount
/// of detected features develop over time in an activity.
pub struct IntegralHist
{
    action_name:     String,
    cluster_centers: Mat,
    feature_hists:   Vec<FeatureHist>
}

impl IntegralHist
{
    pub fn action_name(&self) -> &str { &self.action_name }

    pub fn cluster_centers(&self) -> &Mat { &self.cluster_centers }

    pub fn feature_hists(&self) -> &Vec<FeatureHist> { &self.feature_hists }

    pub fn into_feature_hists(self) -> Vec<FeatureHist> { self.feature_hists }

    /// Converts a list of sample actions (represented as a list of
    /// frames/feature histograms) into a ``max_frames`` long list where
    /// each item is a collection of the feature histogram of each sample at
    /// that frame index.
    ///
    /// Following is a visual representation of the conversion 
    /// ([] = Vec, || = Mat): 
    /// [[fa1, fa2, fa3], [fb1, fb2, fb3]] => [|fa1|, |fa2|, |fa3|
    ///                                        |fb1|  |fb2|  |fb3|]
    fn group_samples_per_frame(
        actions: impl Iterator<Item = IntegralHist>,
        max_frames: usize
    ) -> Vec<Mat>
    {
        actions.fold(
            vec![Mat::default(); max_frames],
            |mut vec_of_mats, sample_action| {
                use crate::take_pad_last::TakePadLast;

                vec_of_mats
                    .iter_mut()
                    .zip(sample_action.into_feature_hists().take_pad_last(max_frames))
                    .for_each(|(mat, sample_fhist)| {
                        mat.push_back(&Mat::from(&sample_fhist))
                            .expect("Failed to push row (feature histogram) under a matrix.");
                    });
                vec_of_mats
            }
        )
    }
}

/// Use this method to calculate an integral histogram for a mean model action.
///
/// Finds the cluster centers for all given sample features, then uses those to
/// calculate a new mean integral histogram over the set of sample actions.
/// Essentially calculating a "general" action and describing it as an integral
/// histogram.
///
/// The covariance matrix over the set of samples, for each frame, is calculated
/// as well.
impl From<(String, Vec<ActionDescription>)> for IntegralHist
{
    fn from((main_action_name, sample_actions): (String, Vec<ActionDescription>)) -> Self
    {
        let acc_action = sample_actions.iter().fold(
            ActionDescription {
                name:     main_action_name.clone(),
                features: Vec::default()
            },
            |mut acc, action| {
                acc.features.append(&mut action.features.clone());
                acc
            }
        );
        let main_action_cluster_centers = acc_action.calc_clusters();
        let sample_actions = sample_actions
            .into_iter()
            .map(|sample| IntegralHist::from((sample, main_action_cluster_centers.clone())));
        let max_frames = acc_action.features.iter().map(Stip::t).max().unwrap();

        // For each frame ``[0..max_frames]`` we have to calculate a covariance matrix
        // of the feature histogram. Our samples are the feature histograms of
        // each sample action at a specific frame. To do all this, we'll first
        // have to create a list of collections of samples.
        let samples_per_frame = IntegralHist::group_samples_per_frame(sample_actions, *max_frames);
        assert_eq!(
            *max_frames,
            samples_per_frame.len(),
            "The amount of feature histograms for action {} did not match the amount of \
             collections of sample feature histograms.",
            &main_action_name
        );

        let mean_fhists = samples_per_frame
            .into_iter()
            .map(FeatureHist::from)
            .collect();

        IntegralHist {
            action_name:     main_action_name,
            cluster_centers: main_action_cluster_centers,
            feature_hists:   mean_fhists
        }
    }
}

/// Calculate an integral histograms for 1 specific observation using the given
/// cluster centers/words/buckets.
///
/// Integral histograms of specific observations have to be created using the
/// cluster centers of the action which the observation will be
/// compared with. This requirement must hold as otherwise the buckets/words of
/// the feature histograms which are being compared are
/// not of the same type, resulting in a nonsensical comparison between the
/// histograms (i.e. comparing apples and oranges).
impl From<(ActionDescription, Mat)> for IntegralHist
{
    fn from((action, cluster_centers): (ActionDescription, Mat)) -> Self
    {
        let mut cur_feature_hist = FeatureHist::default();
        let ActionDescription {
            name: action_name,
            features
        } = action;
        let mut feature_hists: Vec<FeatureHist> = Vec::default();

        let mut matcher = FeatureMatcher::from(cluster_centers.clone());
        let labels = matcher.label_features(&features);

        assert_eq!(
            features.len(),
            labels.len(),
            "The amount of features and corresponding labels do not match."
        );

        // A feature histogram describes the features which have happened up till now.
        // This means we skip the very first frame, i.e. frame 0, as nothing can have
        // happened prior to it.
        let mut cur_frame = 1;
        for (stip, bucket_idx) in features.iter().zip(labels)
        {
            // Add calculated feature hist for each frame up till now. Also adds histograms
            // for frames where no new feature was found, essentially creating a flat part
            // in the function which describes the evolution of features being detected.
            while cur_frame < *stip.t()
            {
                feature_hists.push(cur_feature_hist.clone());
                cur_frame += 1;
            }

            cur_feature_hist.buckets[bucket_idx] += 1.0;
        }
        feature_hists.push(cur_feature_hist);

        IntegralHist {
            action_name,
            cluster_centers,
            feature_hists
        }
    }
}
