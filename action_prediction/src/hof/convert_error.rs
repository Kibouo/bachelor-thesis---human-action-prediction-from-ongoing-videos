use std::{convert::From, error::Error, fmt};

/// Custom error for failed from conversion of a slice to a HOF.
#[derive(Debug)]
pub struct ConvertHofError
{
    description: String
}

impl Error for ConvertHofError
{
    fn description(&self) -> &str { &self.description }
}

impl fmt::Display for ConvertHofError
{
    fn fmt(
        &self,
        f: &mut fmt::Formatter
    ) -> fmt::Result
    {
        write!(f, "{}", self.description())
    }
}

impl From<std::option::NoneError> for ConvertHofError
{
    fn from(_: std::option::NoneError) -> Self
    {
        ConvertHofError {
            description: String::from("None item encountered")
        }
    }
}
