pub mod convert_error;

use convert_error::ConvertHofError;
use std::convert::TryFrom;

// stip-2.0-linux binary bucket size.
pub const HOF_AMT_BUCKETS: usize = 5;

/// Histogram of Optical Flow as used by the stip-2.0-linux binary found at
/// https://github.com/anirudhhkumarr/action-recognition/tree/master/stip-2.0-linux.
#[derive(Clone, PartialEq, Default)]
pub struct Hof([f32; HOF_AMT_BUCKETS]);

impl std::ops::Deref for Hof
{
    type Target = [f32; HOF_AMT_BUCKETS];

    fn deref(&self) -> &Self::Target { &self.0 }
}

impl std::ops::DerefMut for Hof
{
    fn deref_mut(&mut self) -> &mut [f32; HOF_AMT_BUCKETS] { &mut self.0 }
}

impl TryFrom<&[f32]> for Hof
{
    type Error = ConvertHofError;

    fn try_from(value: &[f32]) -> Result<Self, Self::Error>
    {
        let mut hof = Hof::default();
        for i in 0..HOF_AMT_BUCKETS
        {
            hof[i] = *value.get(i)?;
        }

        Ok(hof)
    }
}
