use std::{convert::From, error::Error, fmt};

/// Custom error for failed from conversion of a slice to a HOG.
#[derive(Debug)]
pub struct ConvertHogError
{
    description: String
}

impl Error for ConvertHogError
{
    fn description(&self) -> &str { &self.description }
}

impl fmt::Display for ConvertHogError
{
    fn fmt(
        &self,
        f: &mut fmt::Formatter
    ) -> fmt::Result
    {
        write!(f, "{}", self.description())
    }
}

impl From<std::option::NoneError> for ConvertHogError
{
    fn from(_: std::option::NoneError) -> Self
    {
        ConvertHogError {
            description: String::from("None item encountered")
        }
    }
}
