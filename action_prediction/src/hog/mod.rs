pub mod convert_error;

use convert_error::ConvertHogError;
use std::convert::TryFrom;

// stip-2.0-linux binary bucket size.
pub const HOG_AMT_BUCKETS: usize = 4;

/// Histogram of gradients as used by the stip-2.0-linux binary found at
/// https://github.com/anirudhhkumarr/action-recognition/tree/master/stip-2.0-linux.
#[derive(Clone, Default, PartialEq)]
pub struct Hog([f32; HOG_AMT_BUCKETS]);

impl std::ops::Deref for Hog
{
    type Target = [f32; HOG_AMT_BUCKETS];

    fn deref(&self) -> &Self::Target { &self.0 }
}

impl std::ops::DerefMut for Hog
{
    fn deref_mut(&mut self) -> &mut [f32; HOG_AMT_BUCKETS] { &mut self.0 }
}

impl TryFrom<&[f32]> for Hog
{
    type Error = ConvertHogError;

    fn try_from(value: &[f32]) -> Result<Self, Self::Error>
    {
        let mut hog = Hog::default();
        for i in 0..HOG_AMT_BUCKETS
        {
            hog[i] = *value.get(i)?;
        }

        Ok(hog)
    }
}

impl std::ops::Add for &Hog
{
    type Output = Hog;

    fn add(
        self,
        other: &Hog
    ) -> Self::Output
    {
        let mut res = Hog::default();
        for i in 0..HOG_AMT_BUCKETS
        {
            res[i] = self[i] + other[i];
        }

        res
    }
}
