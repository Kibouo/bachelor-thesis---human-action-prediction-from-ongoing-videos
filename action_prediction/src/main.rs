#![feature(int_error_internals)]
#![feature(try_trait)]
#![feature(impl_trait_in_bindings)]
#![feature(trait_alias)]

#[macro_use]
extern crate cached;

mod action_description;
mod feature_matcher;
mod help_fn;
mod hist;
mod hof;
mod hog;
mod matrix;
mod prediction_fn;
mod stip;
mod take_pad_last;

use crate::{help_fn::*, hist::integral_hist::IntegralHist, prediction_fn::*};

fn main()
{
    use std::env::args;

    let training_file = args().nth(1).expect("No training data filename provided.");
    let model_actions: Vec<IntegralHist> = train_action_models(&training_file);

    println!("Training has finished.");

    // Handle queries coming in through stdin.
    let stdin = std::io::stdin();
    loop
    {
        let observ_data = parse_input(stdin.lock());

        // Prevent the next operations from failing due to currently no query being
        // passed to the program.
        if observ_data.is_empty()
        {
            continue
        }

        let observ_action_summed_hogs = sum_hogs_per_stip(observ_data).next().unwrap();

        split_action_per_second(observ_action_summed_hogs)
            .into_iter()
            .enumerate()
            .for_each(|(second, second_of_observation)| {
                // We can calculate the probability of a certain action occurring in the video.
                // By repeating this process for each possible action that we
                // know of, i.e. for each model action, and selecting the one with the highest
                // resulting probability tells us the action which is, most likely, happening
                // in the query video.
                let action_probabilities = calc_action_prob(&model_actions, second_of_observation);
                let (action, prob) = max_probable_action(action_probabilities);
                println!(
                    "At {} second(s) the action is going to be '{}' with a probability of {}.",
                    second + 1,
                    action,
                    prob
                );
            });
    }
}
