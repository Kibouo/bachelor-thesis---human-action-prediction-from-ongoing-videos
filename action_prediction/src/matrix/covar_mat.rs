use crate::matrix::{Dimensions, IntoInner, Indexing, MatrixOps};
use opencv::core::{COVAR_NORMAL, COVAR_ROWS}; 
use crate::matrix::mat::Mat;
use opencv::core::{COVAR_USE_AVG,
calc_covar_matrix,
CV_32FC1};

const COVAR_FLAGS: i32 = COVAR_NORMAL + COVAR_ROWS;

/// A special type of ``Mat``. The result of calculating the auto-covariance
/// matrix of a set of samples.
pub struct CovarMat(opencv::core::Mat);

impl IntoInner for CovarMat { fn into_inner(self) -> opencv::core::Mat { self.0 } }

impl Dimensions for CovarMat {}

impl Indexing for CovarMat {}

impl MatrixOps<'_> for CovarMat { type MatrixType = CovarMat; }

impl CovarMat
{
    /// Generally we consider a matrix to be singular, this means it is not
    /// invertible, when its determinant is 0. However, calculating the
    /// determinant takes long, especially for big matrices.
    ///
    /// Generally this is solved by LU decomposing the matrix and taking the
    /// product of the diagonal elements. The problem with this is that we work
    /// with floating point numbers, sometimes resulting in extremely small
    /// determinants where they should be 0.
    ///
    /// Luckily a faster and more accurate way to determine whether a matrix is
    /// singular exists. When 2 rows or columns of a matrix are linearly
    /// dependant, one of them can be eliminated to be all 0's. This in its turn
    /// means that the determinant will be 0.
    ///
    /// Thus, by calculating the rank of the matrix and checking whether it's
    /// smaller than the smallest dimension (i.e. checking for linear dependant
    /// rows/cols), we can check whether a matrix is singular.
    pub fn is_singular(&mut self) -> bool
    {
        use std::{cmp::min, f32::EPSILON};

        let rows = self.rows();
        let cols = self.cols();

        Into::<nalgebra::base::DMatrix<f32>>::into(self).svd(false, false).rank(EPSILON) < min(rows, cols)
    }

    /// Calculating the determinant of a matrix with big dimensions takes a long
    /// time to compute. However, another problem which arises is the value of
    /// the determinant itself. For big matrices it can easily become too big
    /// and overflow.
    ///
    /// While the determinant might be large, its log will most likely be small.
    /// We can bring the log inside the determinant calculation as one can see
    /// on the webpage mentioned below.
    ///
    /// For more info read:
    /// https://blogs.sas.com/content/iml/2012/10/31/compute-the-log-determinant-of-a-matrix.html
    pub fn ln_determinant(&mut self) -> f32
    {
        let cols = self.cols(); 

        let lower_triangular = Mat::from((Into::<nalgebra::base::DMatrix<f32>>::into(self).cholesky().unwrap().l(), cols));

        2.0 * lower_triangular.main_diag().ln().sum()
    }

    /// Inverts the matrix using Cholesky decomposition. The base matrix cannot be singular.
    pub fn invert(&mut self) -> Mat
    {
        let cols = self.cols();

        // NOTE: OpenCV's invert function is REALLY slow so we use nalgebra
        Mat::from(&Into::<nalgebra::base::DMatrix<f32>>::into(self).try_inverse().unwrap().transpose().as_slice().chunks(cols).map(|chunk| chunk.to_vec()).collect::<Vec<Vec<f32>>>())
    }

    /// Calculate the auto-covariance matrix of a matrix of samples, optionally
    /// passing a pre-calculated mean to speed up calculations. Returns the
    /// calculated covariance matrix, as well as the mean vector.
    ///
    /// In the case that the resulting matrix would be singular, a small
    /// constant is added to the main diagonal. This can occur when the sample
    /// data is small (both in values of the samples as the amount of
    /// samples). An example is at the start of videos; not many feature
    /// will have been detected up till that point.
    ///
    /// Each row of the ``samples`` matrix must be a sample random vector.
    /// One can optionally pass a mean vector, as a matrix with 1 row, to speed
    /// up calculations.
    ///
    /// For more info read:
    /// https://stackoverflow.com/questions/13145948/how-to-find-out-if-a-matrix-is-singular
    pub fn calc_auto_covar_matrix(
        samples: &Mat,
        mean_opt: Option<Mat>
    ) -> (CovarMat, Mat)
    {
        let mut covar_flags = COVAR_FLAGS;
        let mut covar = CovarMat::default();
        let mean = match mean_opt
        {
            Some(m) =>
            {
                covar_flags += COVAR_USE_AVG;
                m
            }
            None => Mat::default()
        };

        calc_covar_matrix(&samples, &covar, &mean, covar_flags, CV_32FC1)
            .expect("Error during calculation of covariance matrix.");

        // Multivariate Gaussian distr. requires a positive definite covar matrix, which
        // a singular matrix cannot be.
        // When a matrix is singular, its determinant becomes 0.
        if covar.is_singular()
        {
            // Every element of the covariance matrix says something about the correlation
            // between the random variables which contributed to the calculation of it. I.e.
            // the element with position (i,j) states the level of dependency
            // between the i-th and the j-th random variable of our sample vectors.
            //
            // In our case a random variable of a sample vector is a feature bucket/word.
            //
            // By adding a matrix with a small constant on the diagonal, and 0 elsewhere, we
            // effectively state that the values of a word/bucket are somehow correlated. It
            // also states that different buckets are not correlated/dependant on each
            // other. This seems a reasonable assumption for our use-case.
            covar = CovarMat::from((&covar + &Mat::eye(covar.rows(), covar.cols())).into_inner());
        }

        // We do not know the true mean of the population. Having to estimate the mean
        // will result in a biased estimation for the variance. To compensate for this
        // we over-estimate the variance by dividing by ``N - 1`` as opposed to ``N``.
        (CovarMat::from((&covar / (samples.rows() - 1) as f32).into_inner()), mean)
    }
}

impl std::ops::Div<f32> for &CovarMat
{
    type Output = CovarMat;

    fn div(
        self,
        rhs: f32
    ) -> Self::Output
    {
        use opencv::core::divide;

        let lhs = self;
        let rhs = {
            let mut res_mat: Vec<Vec<f32>> = Vec::with_capacity(lhs.rows());
            for _ in 0..lhs.rows()
            {
                let res_row: Vec<f32> = vec![rhs; lhs.cols()];
                res_mat.push(res_row);
            }

            Self::Output::from(&res_mat)
        };
        let res = Self::Output::default();

        divide(lhs, &rhs, &res, 1.0, -1).unwrap();

        res
    }
}

impl std::ops::Add<&Mat> for &CovarMat
{
    type Output = Mat;

    fn add(
        self,
        rhs: &Mat
    ) -> Self::Output
    {
        use opencv::core::add;

        let lhs = self;
        let res = Mat::default();
        let mask = Mat::default();

        add(lhs, rhs, &res, &mask, -1).unwrap();

        res
    }
}

impl std::ops::Add<&CovarMat> for &CovarMat
{
    type Output = CovarMat;

    fn add(
        self,
        rhs: &CovarMat
    ) -> Self::Output
    {
        use opencv::core::add;

        let lhs = self;
        let res = CovarMat::default();
        let mask = CovarMat::default();

        add(lhs, rhs, &res, &mask, -1).unwrap();

        res
    }
}

impl Default for CovarMat {
    fn default() -> Self { Self(opencv::mat()) }
}

impl Clone for CovarMat
{
    fn clone(&self) -> Self { Self(self.0.clone().unwrap()) }
}

impl std::ops::Deref for CovarMat
{
    type Target = opencv::core::Mat;

    fn deref(&self) -> &Self::Target { &self.0 }
}

impl std::ops::DerefMut for CovarMat
{
    fn deref_mut(&mut self) -> &mut opencv::core::Mat { &mut self.0 }
}

impl From<opencv::core::Mat> for CovarMat
{
    fn from(m: opencv::core::Mat) -> Self { Self(m) }
}

impl From<&Vec<Vec<f32>>> for CovarMat
{
    fn from(v: &Vec<Vec<f32>>) -> Self
    {
        CovarMat::from(crate::matrix::vec2d_to_mat_f32(v))
    }
}

impl Into<Vec<Vec<f32>>> for &mut CovarMat
{
    fn into(self) -> Vec<Vec<f32>>
    {
        let mut res_mat: Vec<Vec<f32>> = Vec::with_capacity(self.rows());
        (0..self.rows()).for_each(|r| {
            let res_row: Vec<f32> = (0..self.cols())
                .map(|c| *self.at_2d(r, c))
                .collect();

            res_mat.push(res_row);
        });
        res_mat
    }
}

impl Into<nalgebra::base::DMatrix<f32>> for &mut CovarMat {
    fn into(self) -> nalgebra::base::DMatrix<f32> {
        nalgebra::base::DMatrix::from_vec(self.rows(), self.cols(), {
            let vec_2d: Vec<Vec<f32>> = self.into();
            vec_2d.into_iter().flatten().collect()
        })
    }
}
