use crate::matrix::{Dimensions, Indexing, IntoInner, MatrixOps};

/// A wrapper for OpenCV's ``Mat`` type. This allows us to implement traits,
/// as well as detach our code from a specific OpenCV wrapper/library.
///
/// ``Mat`` dereferences to an OpenCV ``Mat`` which means every OpenCV method
/// can directly be called on it.
pub struct Mat(opencv::core::Mat);

impl IntoInner for Mat { fn into_inner(self) -> opencv::core::Mat { self.0 } }

impl Dimensions for Mat {}

impl Indexing for Mat {}

impl MatrixOps<'_> for Mat { type MatrixType = Mat; }

impl Mat
{
    /// Creates a matrix filled with the given default number.
    pub fn new(
        rows: usize,
        cols: usize,
        default: f32
    ) -> Mat
    {
        Mat::from(&vec![vec![default; cols]; rows])
    }
   
    /// Returns an identity matrix with the specified dimensions.
    pub fn eye(
        rows: usize,
        cols: usize
    ) -> Mat
    {
        let mut res_mat: Vec<Vec<f32>> = Vec::with_capacity(rows);
        (0..rows).for_each(|r| {
            let res_row: Vec<f32> = (0..cols)
                .map(|c| {
                    if c == r
                    {
                        1.0
                    }
                    else
                    {
                        0.0
                    }
                })
                .collect();

            res_mat.push(res_row);
        });

        Mat::from(&res_mat)
    }
}

impl Default for Mat
{
    fn default() -> Self { Mat::from(opencv::mat()) }
}

impl Clone for Mat
{
    fn clone(&self) -> Self { Self(self.0.clone().unwrap()) }
}

impl std::ops::Mul for &Mat
{
    type Output = Mat;

    fn mul(
        self,
        rhs: &Mat
    ) -> Self::Output
    {
        let lhs = self;
        assert_eq!(
            lhs.cols(),
            rhs.rows(),
            "Can't multiply matrices with dimensions i*{} and {}*j",
            lhs.cols(),
            rhs.rows()
        );

        let mut res_mat: Vec<Vec<f32>> = Vec::with_capacity(lhs.rows());
        for r in 0..lhs.rows()
        {
            let mut res_row: Vec<f32> = Vec::with_capacity(rhs.cols());
            for c in 0..rhs.cols()
            {
                res_row.push(lhs.row(r).dot(&rhs.col(c).transpose()).unwrap() as f32);
            }

            res_mat.push(res_row);
        }

        Mat::from(&res_mat)
    }
}

impl std::ops::Deref for Mat
{
    type Target = opencv::core::Mat;

    fn deref(&self) -> &Self::Target { &self.0 }
}

impl std::ops::DerefMut for Mat
{
    fn deref_mut(&mut self) -> &mut opencv::core::Mat { &mut self.0 }
}

impl From<opencv::core::Mat> for Mat
{
    fn from(m: opencv::core::Mat) -> Self { Self(m) }
}

impl From<&Vec<Vec<f32>>> for Mat
{
    fn from(v: &Vec<Vec<f32>>) -> Self
    {
        Mat::from(crate::matrix::vec2d_to_mat_f32(v))
    }
}

impl From<&crate::hog::Hog> for Mat
{
    fn from(h: &crate::hog::Hog) -> Self
    {
        use std::ops::Deref;
        Mat::from(crate::matrix::vec2d_to_mat_f32(&[h.deref()]))
    }
}

impl From<&crate::hist::feature_hist::FeatureHist> for Mat
{
    fn from(f: &crate::hist::feature_hist::FeatureHist) -> Self
    {
        Mat::from(crate::matrix::vec2d_to_mat_f32(&[&f.buckets[..]]))
    }
}

impl From<(nalgebra::base::DMatrix<f32>, usize)> for Mat {
    fn from((f, dim): (nalgebra::base::DMatrix<f32>, usize)) -> Self {
        Mat::from(&f
                .transpose()
                .as_mut_slice()
                .chunks(dim)
                .map(|c| c.to_vec())
                .collect::<Vec<Vec<f32>>>())
    }
}

impl Into<nalgebra::base::DMatrix<f32>> for &mut Mat {
    fn into(self) -> nalgebra::base::DMatrix<f32> {
        nalgebra::base::DMatrix::from_vec(self.rows(), self.cols(), {
            let vec_2d: Vec<Vec<f32>> = self.into();
            vec_2d.into_iter().flatten().collect()
        })
    }
}

impl Into<Vec<Vec<f32>>> for &mut Mat
{
    fn into(self) -> Vec<Vec<f32>>
    {
        let mut res_mat: Vec<Vec<f32>> = Vec::with_capacity(self.rows());
        (0..self.rows()).for_each(|r| {
            let res_row: Vec<f32> = (0..self.cols())
                .map(|c| *self.at_2d(r, c))
                .collect();

            res_mat.push(res_row);
        });
        res_mat
    }
}
