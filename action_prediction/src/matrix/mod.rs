pub mod covar_mat;
pub mod mat;

use crate::matrix::mat::Mat;
use std::ops::{Deref, DerefMut};

pub trait DerefMat = Deref<Target = opencv::core::Mat>;
pub trait DerefMutMat = DerefMut<Target = opencv::core::Mat>;
pub trait IntoNalgMat = Into<nalgebra::base::DMatrix<f32>>;
pub trait FromVec2D<'v> = From<&'v Vec<Vec<f32>>>;

// Based on https://github.com/twistedfall/opencv-rust/issues/20
pub fn vec2d_to_mat_f32(s: &[impl AsRef<[f32]>]) -> opencv::core::Mat
{
    let row_count: i32 = s.len() as _;
    let col_count: i32 = if row_count > 0
    {
        s[0].as_ref().len() as _
    }
    else
    {
        0
    };
    let mut out = Mat::from(
        opencv::core::Mat::new_rows_cols(row_count, col_count, opencv::core::CV_32F).unwrap()
    );
    for (row_n, row) in s.iter().enumerate()
    {
        for (col_n, val) in row.as_ref().iter().enumerate()
        {
            *out.at_2d(row_n as _, col_n as _) = *val;
        }
    }

    out.into_inner()
}

pub trait Dimensions: DerefMat
{
    fn rows(&self) -> usize { self.size().expect("Unable to get size of matrix.").height as usize }
    fn cols(&self) -> usize { self.size().expect("Unable to get size of matrix.").width as usize }
}

pub trait IntoInner: Sized
{
    /// Consumes the object and returns its owned data.
    fn into_inner(self) -> opencv::core::Mat;
}

pub trait Indexing: DerefMat + DerefMutMat
{
    // TODO: make a non-mutable version. Currently a lot of ``From`` implementations
    // for the mats, and functions which use those e.g. ln_determinant, are only
    // mutable because they end up requiring this.
    #[allow(clippy::cast_ptr_alignment)]
    fn at_2d(
        &mut self,
        row: usize,
        col: usize
    ) -> &mut f32
    {
        unsafe {
            self.ptr1(row as i32, col as i32).and_then(|x| {
                (x as *mut f32)
                    .as_mut()
                    .ok_or_else(|| "Got null pointer".to_string())
            })
        }
        .unwrap()
    }

    fn row(
        &self,
        r: usize
    ) -> Mat
    {
        Mat::from(self.deref().row(r as i32).unwrap())
    }

    fn col(
        &self,
        c: usize
    ) -> Mat
    {
        Mat::from(self.deref().col(c as i32).unwrap())
    }
}

pub trait MatrixOps<'v>: Dimensions + IntoInner
{
    type MatrixType: Default + FromVec2D<'v> + DerefMat;

    /// Computes natural log for each element of the matrix.
    fn ln(&self) -> Self::MatrixType
    {
        use opencv::core::log;

        let res = Self::MatrixType::default();
        log(self, &res).unwrap();

        res
    }

    /// Returns a column matrix with the elements of the main diagonal.
    fn main_diag(&self) -> Mat { Mat::from(self.deref().diag(0).unwrap()) }

    /// Takes the sum of all components.
    fn sum(&self) -> f32
    {
        use opencv::core::sum;

        sum(self).unwrap().data[0] as f32
    }

    /// Transposes the matrix
    fn transpose(&self) -> Self::MatrixType
    {
        use opencv::core::transpose;

        let trans = Self::MatrixType::default();
        transpose(self, &trans).unwrap();

        trans
    }
}
