use crate::{action_description::ActionDescription,
            hist::{feature_hist::FeatureHist, integral_hist::IntegralHist},
            matrix::covar_mat::CovarMat, matrix::mat::Mat};
// TODO: SizedCache is implemented inefficiently in the library. 
// Once it's fixed, replace UnboundCache with it.
use cached::UnboundCache;

// Amount of frames per fixed time unit duration. By fixing durations over the
// observation, instead of dynamically searching the best fit, the complexity of
// the likelihood computation goes down significantly.
pub const FIXED_OBSERV_DURATION: f32 = 25.0;
const DYNAMIC_ACTION_SIMILARITY_CACHE_SIZE: usize = 4096;

/// Returns for each possible action the probability that it is going to
/// appear in the observation.
///
/// By combining Bayes theorem and the law of total probability, we are able to
/// calculate the probability of a certain action to occur.
///
/// Note that in general the bottom of Bayes' rule is a constant throughout all possible action
/// models, i.e. it does not depend on the observation. In our case it does vary slightly due to
/// the observation being clustered using the currently tested action ``p``'s cluster centers. This
/// is not a significant error though and thus we still drop it to speed up calculations.
/// To still get probabilities, we use simple percentage calculation.
pub fn calc_action_prob(
    model_actions: &[IntegralHist],
    observ_action: ActionDescription
) -> Vec<(String, f64)>
{
    let likelihoods = model_actions
        .iter()
        .map(|test_model_action| {
            (
                test_model_action.action_name().to_string(),
                action_likelihood(
                    model_actions.len(),
                    test_model_action,
                    &IntegralHist::from((
                        observ_action.clone(),
                        test_model_action.cluster_centers().clone()
                    ))
                )
            )
        })
        .collect::<Vec<(String, f64)>>();

    let sum = likelihoods.iter().fold(0.0, |acc, (_, p)| acc + p);

    likelihoods.into_iter().map(|(action_name, p)| (action_name, p / sum)).collect::<Vec<(String, f64)>>()
}

/// Finds the action with the highest probability in a list of ``(action_name,
/// probability)`` tuples.
pub fn max_probable_action(action_probabilities: Vec<(String, f64)>) -> (String, f64)
{
    action_probabilities
        .into_iter()
        .max_by(|(_, x), (_, y)| {
            x.partial_cmp(y)
                .unwrap_or_else(|| panic!("Unable to compare floats {} and {}", x, y))
        })
        .expect("No model actions to compare to.")
}

/// The likelihood of an action. This is the sum over all possible progress
/// levels ``d`` of the likelihood of an action at a single moment, taking into
/// consideration the similarity of the observation's length and the current
/// progress level, as well as the prior possibility.
fn action_likelihood(
    total_amt_actions: usize,
    test_ihist: &IntegralHist,
    observ_ihist: &IntegralHist
) -> f64
{
    use crate::take_pad_last::TakePadLast;

    // TODO: figure out how to properly handle continuous video stream
    let test_fhists = test_ihist.feature_hists();
    let observ_fhists = observ_ihist.feature_hists();
    // Make sure there are enough frames to take ``n`` FIXED_OBSERV_DURATION steps
    // during analysis.
    let amt_frames_observ = {
        let rest = observ_fhists.len() as f32 % FIXED_OBSERV_DURATION;
        if rest != 0.0
        {
            (observ_fhists.len() as f32 - rest + FIXED_OBSERV_DURATION).ceil() as usize
        }
        else
        {
            observ_fhists.len()
        }
    };
    let observ_fhists = &observ_fhists
        .iter()
        .cloned()
        .take_pad_last(amt_frames_observ)
        .collect::<Vec<FeatureHist>>()[..];

    let amt_frames_model = test_fhists.len();
    (0..amt_frames_model)
        .map(|cur_frame| {
            // likelihood fn
            let a_sim = dynamic_action_similarity(
                test_ihist.action_name(),
                observ_ihist.action_name(),
                &test_fhists[..=cur_frame],
                observ_fhists
            );
            // TODO: remove non-dynamic version. For testing only.
            //let a_sim = action_similarity(
            //     test_fhists[cur_frame].clone(),
            //     test_fhists[cur_frame].covar().clone().unwrap(),
            //     observ_fhists
            //        .iter()
            //        .cloned()
            //        .take_pad_last(amt_frames_model)
            //        .collect::<Vec<FeatureHist>>()[cur_frame]
            //        .clone()
            //);
            // likelihood prog. lvl
            let p_sim = progress_similarity(amt_frames_observ, cur_frame + 1);
            // uniform prior prob.
            let prior = 1.0 / (total_amt_actions * amt_frames_model) as f64;

            a_sim * p_sim * prior
        })
        .fold(0.0, |acc, prob| acc + prob)
}

/// Human actions are sequential and temporally dynamic. Both concepts are
/// thought of as normal in day-to-day life.
///
/// Sequentiality is easily explained by looking at a video of an action. The
/// first few frames contain part of an action which have to have happened
/// before the latter frames/parts of the action can happen. We can leverage
/// this property to improve our prediction process.
///
/// Dynamic temporality stems from the fact that we are human. It is impossible
/// to repeat the same action at the same exact speed at every step and make it
/// last exactly the same duration. Again we can think of this in terms of a
/// video recording. Take 2 videos of the same person performing the same
/// action. Comparing the ``n``-th frame, one will see that their body is not
/// exactly in the same position, be it by even a millimeter. This observation
/// becomes easier to make at higher frame-rates.
///
/// We implement the above 2 concepts in our prediction algorithm as follows:
/// * Sequentiality: we split our observation into sub-intervals. Every interval
///   we calculate the probability of the action occurring at that point in
///   time. This is the same as before. However, this time we combine the
///   current probability with that of the previous interval.
/// * Dynamic temporality: to incorporate different speeds of enactments of an
///   action, we split the model into sub-intervals as well. Now, when matching
///   we compare 2 intervals. We do this for every possible combination of
///   interval lengths. The maximum of these probabilities is chosen as the
///   actual probability of the action happening at that time-point.
///
/// Note: to improve performance we lock the interval size of the observation on
/// 1 second, or 24 frames with our recording settings.
///
/// Both of these concepts are implemented as a dynamic algorithm. A dynamic
/// algorithm is an algo which finds the optimal solution to a problem by
/// breaking it up into overlapping sub-problems. These sub-problems are solved
/// recursively by the same algorithm and each produce an optimal solution for
/// their problem. They are also overlapping which means the same sub-problems
/// are produced multiple times. E.g. calculating the 5-th Fibonacci number
/// produces the sub-problem of calculating the 1-st Fibonacci number several
/// times.
cached_key! {
    DYNAMIC_ACTION_SIMILARITY_CACHE: UnboundCache<(String, String, usize, usize), f64> =
        UnboundCache::with_capacity(DYNAMIC_ACTION_SIMILARITY_CACHE_SIZE);
    Key = { (test_action_name.to_string(), observ_action_name.to_string(), test_fhists.len(), observ_fhists.len()) };
    fn dynamic_action_similarity(
        test_action_name: &str,
        observ_action_name: &str,
        test_fhists: &[FeatureHist],
        observ_fhists: &[FeatureHist]
    ) -> f64 =
    {
        let end_frame_model = test_fhists.len();
        let end_frame_observ = observ_fhists.len();

        if end_frame_model < 1 || end_frame_observ < FIXED_OBSERV_DURATION as usize
        {
            return 1.0
        }

        (0..end_frame_model)
            .map(|dyn_delta| -> decorum::R64 {
                let end_frame_sub_model = end_frame_model - dyn_delta;
                let end_frame_sub_observ = end_frame_observ - FIXED_OBSERV_DURATION as usize;

                let sub_ans = dynamic_action_similarity(
                    test_action_name,
                    observ_action_name,
                    &test_fhists[..end_frame_sub_model],
                    &observ_fhists[..end_frame_sub_observ]
                );

                // lhs ``L`` and rhs ``R`` are  assumed to be independent identically distributed standard
                // normal variables. Using the rules of sum & linearity of normal distr we
                // can see that the distr of ``L - R`` evaluates to: ```
                // L - R
                // <=> L + a * R where a = -1
                // => N(meanL + a meanR, cov_matL + a^2 * cov_matR)
                // <=> N(meaL - meanR, cov_matL + cov_matR)
                // ```
                // Knowing the above, to calculate the (multivariate) normal distr of an interval of
                // feature histograms, we take the difference between their means and add up their
                // covariance matrices.
                let (diff_mean, sum_cov) = {
                    let lhs = &test_fhists[end_frame_model - 1];
                    let rhs = &test_fhists[end_frame_sub_model - 1];

                    (
                        lhs - rhs,
                        lhs.covar().as_ref().unwrap() + rhs.covar().as_ref().unwrap()
                    )
                };

                let similarity_cur_interv = action_similarity(
                    diff_mean,
                    sum_cov,
                    &observ_fhists[end_frame_observ - 1] - &observ_fhists[end_frame_sub_observ]
                );

                (sub_ans * similarity_cur_interv).into()
            })
            .max()
            .unwrap()
            .into()
    }
}

/// This function calculates the similarity between a model action and a query
/// action at a specific time-point by comparing their feature histograms. Also
/// called the likelihood function in our Bayes theorem. Integral histograms of
/// activities are modelled with Gaussian distributions having a uniform
/// variance.
///
/// As feature histograms are vectors of random variables, we work out a
/// multivariate normal distribution.
///
/// The feature histograms have to be calculated for the same progress levels
/// ``d`` to return a sensical answer.
fn action_similarity(
    mean_action: FeatureHist,
    mut covar_mat: CovarMat,
    query_action: FeatureHist
) -> f64
{
    use crate::hist::feature_hist::AMT_VISUAL_WORDS;
    use std::f64::consts::{E, PI};
    use crate::matrix::MatrixOps;
    use crate::matrix::Indexing;

    let base = (2.0 * PI).powf(-(AMT_VISUAL_WORDS as f64) / 2.0);

    // The determinant would be too big to properly take the sqrt of. We circumvent
    // a huge determinant by calculating ``ln(det(covar_mat))``.
    // ``1/sqrt(determinant)`` can then be simplified as:
    // ```
    // det(covar)^(-1/2)
    // <=> (e^(log_det(covar)))^(-1/2)
    // <=> e^(-log_det(covar)/2)
    // ```
    // This allows us to avoid determinants which are too big for us to work with.
    let sqrt_det = E.powf(f64::from(-covar_mat.ln_determinant()) / 2.0);

    let diff_action = Mat::from(&(&query_action - &mean_action));
    let exp = E.powf(
        -0.5 * f64::from(
            *(&(&diff_action * &covar_mat.invert()) * &diff_action.transpose())
                .at_2d(0, 0)
        )
    );

    base * sqrt_det * exp
}

/// A similarity calculation between two progress levels. We are interested in
/// this similarity as a sort of 'compensation value'. It puts into perspective
/// the match % of an observation and model which vary wildly in length.
///
/// Think of following example:
/// our model action takes 10 seconds while our observation takes 2000 seconds.
/// However, the actions match 100% according to the feature analysis. While it
/// is not unlikely that an action differs in duration, such a big difference
/// might be an indication of something else going on.
///
/// This distance metric, while interesting for the relativity factor it
/// provides, is not of big importance. The most important part is still the
/// action likelihood formula. Think of this progress similarity as the
/// 'seasoning' of the algorithm.
fn progress_similarity(
    t: usize,
    d: usize
) -> f64
{
    use std::f64::consts::E;

    let t = t as isize;
    let d = d as isize;
    // euclid distance between 1D points is equal to their absolute difference.
    let euclid_dist = (t - d).abs();

    E.powf(-euclid_dist as f64)
}
