use std::{convert::From, error::Error, fmt};

/// Custom error for failed parsing of a string to a STIP.
#[derive(Debug)]
pub struct ParseStipError
{
    description: String
}

impl ParseStipError
{
    pub fn description(&self) -> &str { &self.description }
}

impl Error for ParseStipError
{
    fn description(&self) -> &str { &self.description }
}

impl fmt::Display for ParseStipError
{
    fn fmt(
        &self,
        f: &mut fmt::Formatter
    ) -> fmt::Result
    {
        write!(f, "{}", self.description())
    }
}

impl Default for ParseStipError
{
    fn default() -> Self
    {
        ParseStipError {
            description: String::from("Invalid STIP description.")
        }
    }
}

impl From<std::num::ParseIntError> for ParseStipError
{
    fn from(error: std::num::ParseIntError) -> Self
    {
        ParseStipError {
            description: String::from(error.__description())
        }
    }
}

impl From<std::option::NoneError> for ParseStipError
{
    fn from(_: std::option::NoneError) -> Self
    {
        ParseStipError {
            description: String::from("None item encountered")
        }
    }
}

impl From<std::num::ParseFloatError> for ParseStipError
{
    fn from(error: std::num::ParseFloatError) -> Self
    {
        ParseStipError {
            description: String::from(error.__description())
        }
    }
}

impl From<crate::hog::convert_error::ConvertHogError> for ParseStipError
{
    fn from(error: crate::hog::convert_error::ConvertHogError) -> Self
    {
        ParseStipError {
            description: error.description().to_string()
        }
    }
}

impl From<crate::hof::convert_error::ConvertHofError> for ParseStipError
{
    fn from(error: crate::hof::convert_error::ConvertHofError) -> Self
    {
        ParseStipError {
            description: error.description().to_string()
        }
    }
}
