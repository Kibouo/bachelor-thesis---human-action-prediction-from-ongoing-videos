/// An iterator over ``n`` items of the base iterator, padding with the last
/// item if the base is not long enough.
pub struct TakePadLastImpl<B>
where
    B: Iterator,
    B::Item: Clone
{
    n:    usize,
    base: B,
    last: Option<B::Item>
}

impl<B> Iterator for TakePadLastImpl<B>
where
    B: Iterator,
    B::Item: Clone
{
    type Item = B::Item;

    fn next(&mut self) -> Option<Self::Item>
    {
        if self.n > 0
        {
            self.n -= 1;
            self.base
                .next()
                .map(|i| {
                    self.last = Some(i.clone());
                    i
                })
                .or_else(|| self.last.clone())
        }
        else
        {
            None
        }
    }
}

pub trait TakePadLast: IntoIterator
{
    /// Makes the iterator ``d`` items long. It drops everything after the
    /// ``d``-th item if the base iterator is too long, or pads by repeating the
    /// last item if the base iterator is too short.
    ///
    /// This is used to extend a list of feature histograms. We are
    /// allowed to extend the list of feature histograms with the last item as
    /// obviously no new features are being detected after the video sequence
    /// has ended. Padded features essentially create a plateau in the
    /// feature evolution function (the function represented by an integral
    /// histogram).
    fn take_pad_last(
        self,
        n: usize
    ) -> TakePadLastImpl<Self::IntoIter>
    where
        Self: IntoIterator + Sized,
        Self::Item: Clone
    {
        TakePadLastImpl {
            n,
            base: self.into_iter(),
            last: None
        }
    }
}

use crate::hist::feature_hist::FeatureHist;
impl<T: ?Sized> TakePadLast for T where T: IntoIterator<Item = FeatureHist> {}
