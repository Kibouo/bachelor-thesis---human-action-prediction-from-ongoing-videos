use std::{collections::HashMap,
          env::args,
          fs::File,
          io::{BufRead, BufReader}};

fn main_action(full_action: &str) -> String { full_action.split('-').next().unwrap().to_owned() }

fn main()
{
    let results_file = args().nth(1).expect("No results data filename provided.");
    let file_reader = BufReader::new(File::open(results_file).unwrap());

    let lines = file_reader
        .lines()
        .map(Result::unwrap)
        .collect::<Vec<String>>();

    let actions = lines.iter().filter(|line| line.contains("seq"));

    let mut results: Vec<HashMap<String, bool>> = Vec::new();
    let mut cur_action = String::new();
    for line in lines.iter()
    {
        if line.contains("seq")
        {
            cur_action = line.clone();
        }
        else
        {
            let mut line = line.split_ascii_whitespace();

            let second = line.nth(0).expect("nth(0)").parse::<usize>().unwrap() - 1;
            let hashmap = match results.get_mut(second)
            {
                Some(hashmap) => hashmap,
                None =>
                {
                    results.insert(second, HashMap::default());
                    results.get_mut(second).expect("inserted get_mut")
                }
            };
            hashmap.insert(
                cur_action.clone(),
                main_action(&cur_action) == line.nth(1).expect("nth(2)")
            );
        }
    }

    // max 4 sec and extend last known result
    let max_4_sec = actions.fold(HashMap::new(), |mut acc, action| {
        acc.insert(
            action,
            results
                .iter()
                .enumerate()
                .fold(Vec::new(), |mut acc, (idx, map)| {
                    let idx = idx + 1;

                    if idx < 5
                    {
                        acc.push(match map.get(action)
                        {
                            Some(result) => result,
                            None =>
                            {
                                let mut i = idx - 1; // cancel print +1
                                let mut last_registered_result = None;

                                while last_registered_result.is_none()
                                {
                                    i -= 1;
                                    last_registered_result = match results.get(i)
                                    {
                                        Some(hashmapy) => hashmapy.get(action),
                                        None => Some(&false)
                                    }
                                }

                                last_registered_result.unwrap()
                            }
                        });
                    }
                    acc
                })
        );
        acc
    });

    // gen graph of 4 secs
    let graph = max_4_sec.iter().fold(
        (0..4).map(|_| (0, 0)).collect::<Vec<(usize, usize)>>(),
        |mut acc, (name, results)| {
            results.iter().enumerate().for_each(|(i, r)| {
                let second_data = acc.get_mut(i).expect("get_mut() second_data");
                if **r
                {
                    second_data.0 += 1
                };
                second_data.1 += 1
            });
            acc
        }
    );

    println!("{:?}", graph);
}
