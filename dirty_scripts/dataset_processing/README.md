Download the zips of the training videos made for the STIP binary: https://www.nada.kth.se/cvap/actions/

Each video in this dataset consists of 4 sequences. The precise frame-bounds are described in the file ``00sequences.txt``, also to be found on the same webpage.

To split these videos, run the script in this directory.