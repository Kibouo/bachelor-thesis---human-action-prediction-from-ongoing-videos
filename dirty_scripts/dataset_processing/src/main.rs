use std::{fs::File,
          io::{prelude::*, BufReader},
          process::Command};
use std::fs::read_dir;
use std::fs::create_dir;

const VID_FRAMERATE: u32 = 25;
const RESULT_DIR: &str = "./result";
const VID_EXT: &str = "avi";
const SRC_VID_DIR: &str = "./vids";

fn check_dirs_valid() {
    read_dir(SRC_VID_DIR).expect("Source dir with videos does not exist.");
    read_dir(RESULT_DIR).map(|_| ()).or_else(|_| create_dir(RESULT_DIR)).expect("Unable to create result directory.");
}

fn split_vid(
    vid_name: &str,
    sequences: Vec<(u32, u32)>
)
{
    check_dirs_valid();

    let core_activity_name = vid_name.split('_').collect::<Vec<&str>>()[1];
    sequences
        .into_iter()
        .enumerate()
        .for_each(|(idx, (start, end))| {
            Command::new("ffmpeg")
                .args(&["-ss", &format!("{}", start as f32 / VID_FRAMERATE as f32)])
                .args(&["-i", &format!(
                    "{}/{}_uncomp.{}",
                    SRC_VID_DIR, vid_name, VID_EXT
                )])
                .args(&["-frames:v", &format!("{}", end - start)])
                .arg(format!(
                    "{}/{}-{}_seq{}.{}",
                    RESULT_DIR, core_activity_name, vid_name, idx, VID_EXT
                ))
                .spawn()
                .unwrap();
        });
}

fn main()
{
    let seq_descrs = BufReader::new(File::open("./00sequences.txt").expect("File '00sequences.txt' not found."));
    seq_descrs
        .lines()
        .map(|l| l.unwrap())
        .filter(|line| line.get(0..6).unwrap_or("line_was_empty") == "person")
        .for_each(|line| {
            let mut line = line.split_ascii_whitespace();
            let video = line.next().unwrap();
            line.next(); // skip 'frames' word
            let sequences = line
                .map(|s| {
                    let mut s = s
                        .trim_end_matches(',')
                        .split('-')
                        .map(str::parse::<u32>)
                        .map(|i| i.unwrap());
                    (s.next().unwrap(), s.next().unwrap())
                })
                .collect::<Vec<(u32, u32)>>();

            split_vid(video, sequences);
        });
}
