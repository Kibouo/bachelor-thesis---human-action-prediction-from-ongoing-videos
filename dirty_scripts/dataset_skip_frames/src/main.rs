use std::{fs::{create_dir, read_dir},
          path::PathBuf,
          process::Command};

const VID_FRAMERATE: f32 = 12.5;
const BITSTREAM_RESULT_DIR: &str = "./bitstream_result/";
const SKIPPED_FRAMES_RESULT_DIR: &str = "./skipped_frames_result/";
const RESULT_DIR: &str = "./result/";
const VID_EXT: &str = "avi";
const RAW_BITSTREAM_EXT: &str = "yuv";
const SRC_VID_DIR: &str = "../dataset_processing/result/";

fn check_dirs_valid()
{
    read_dir(SRC_VID_DIR).expect("Source dir with videos does not exist.");
    read_dir(BITSTREAM_RESULT_DIR)
        .map(|_| ())
        .or_else(|_| create_dir(BITSTREAM_RESULT_DIR))
        .expect("Unable to create bitstream result directory.");
    read_dir(RESULT_DIR)
        .map(|_| ())
        .or_else(|_| create_dir(RESULT_DIR))
        .expect("Unable to create result directory.");
    read_dir(SKIPPED_FRAMES_RESULT_DIR)
        .map(|_| ())
        .or_else(|_| create_dir(SKIPPED_FRAMES_RESULT_DIR))
        .expect("Unable to create result directory.");
}

fn to_raw_bitstream(path: PathBuf)
{
    Command::new("ffmpeg")
        .args(&["-i", path.as_path().to_str().unwrap()])
        .args(&["-an", "-vcodec", "rawvideo", "-pix_fmt", "yuv420p"])
        .arg(format!(
            "{}{}.{}",
            BITSTREAM_RESULT_DIR,
            path.file_name()
                .unwrap()
                .to_str()
                .unwrap()
                .split('.')
                .next()
                .unwrap(),
            RAW_BITSTREAM_EXT
        ))
        .spawn()
        .unwrap()
        .wait()
        .unwrap();
}

fn skip_every_other_frame(path: PathBuf)
{
    Command::new("ffmpeg")
        .args(&[
            "-r",
            "2",
            "-s",
            "160x120",
            "-i",
            path.to_str().unwrap(),
            "-filter:v",
            "select=mod(n-1\\,2)",
            "-c:v",
            "rawvideo",
            "-r",
            "1",
            "-format",
            "rawvideo",
            "-pix_fmt",
            "yuv420p",
            "-an"
        ])
        .arg(format!(
            "{}{}.{}",
            SKIPPED_FRAMES_RESULT_DIR,
            path.file_name()
                .unwrap()
                .to_str()
                .unwrap()
                .split('.')
                .next()
                .unwrap(),
            RAW_BITSTREAM_EXT
        ))
        .spawn()
        .unwrap()
        .wait()
        .unwrap();
}

fn raw_to_avi(path: PathBuf)
{
    Command::new("ffmpeg")
        .args(&[
            "-f",
            "rawvideo",
            "-vcodec",
            "rawvideo",
            "-s",
            "160x120",
            "-r",
            &format!("{}", VID_FRAMERATE),
            "-pix_fmt",
            "yuv420p",
            "-i",
            path.to_str().unwrap(),
            "-c:v",
            "libx264",
            "-preset",
            "ultrafast",
            "-qp",
            "0"
        ])
        .arg(format!(
            "{}{}.{}",
            RESULT_DIR,
            path.file_name()
                .unwrap()
                .to_str()
                .unwrap()
                .split('.')
                .next()
                .unwrap(),
            VID_EXT
        ))
        .spawn()
        .unwrap()
        .wait()
        .unwrap();
}

fn main()
{
    check_dirs_valid();

    // read_dir(SRC_VID_DIR)
    //    .unwrap()
    //    .map(|dir_item| dir_item.unwrap().path())
    //    .for_each(to_raw_bitstream);

    read_dir(BITSTREAM_RESULT_DIR)
        .unwrap()
        .map(|dir_item| dir_item.unwrap().path())
        .for_each(skip_every_other_frame);

    read_dir(SKIPPED_FRAMES_RESULT_DIR)
        .unwrap()
        .map(|dir_item| dir_item.unwrap().path())
        .for_each(raw_to_avi);
}
