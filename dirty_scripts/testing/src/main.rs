extern crate rayon;
extern crate regex;

use rayon::prelude::*;
use regex::Regex;
use std::{env::args,
          fs::{File, OpenOptions},
          io::{self, prelude::*, BufRead, BufReader},
          process::{Command, Stdio}};
use std::{fs::{create_dir, read_dir, remove_dir_all}};

const QUERIES_DIR: &str = "./queries/";

fn check_dirs() {
    read_dir(QUERIES_DIR)
        .map(|_| remove_dir_all(QUERIES_DIR).unwrap());
    create_dir(QUERIES_DIR)
        .expect("Unable to create bitstream result directory.");
}

fn create_query_files(all_data_file: &str)
{
    let file_reader = BufReader::new(File::open(all_data_file).unwrap());
    // of the first 8 ppl we test 2 parts of 2 situations of all  actions. This
    // makes 32 queries per action spread over 8 ppl.
    let re = Regex::new(r"[a-z]*-person0[1-8]_[a-z]*_d[1-2]_seq[0-1]").unwrap();

    let mut query_file: Option<File> = None;
    let mut writing = false;
    file_reader.lines().map(Result::unwrap).for_each(|line| {
        if writing
        {
            if query_file.is_some()
            {
                query_file
                    .as_mut()
                    .unwrap()
                    .write_all(format!("{}\n", line).as_bytes())
                    .unwrap();
            }

            if line == ""
            {
                writing = false;
            }
        }
        else
        {
            writing = re.is_match(&line);
            if writing
            {
                query_file = Some(
                    OpenOptions::new()
                        .create(true)
                        .append(true)
                        .open(&format!("{}{}", QUERIES_DIR, line.get(2..).unwrap()))
                        .unwrap()
                );
                query_file
                    .as_mut()
                    .unwrap()
                    .write_all(format!("# point-type y-norm x-norm t-norm y x t sigma2 tau2 dscr-hog(72)\n{}\n", line).as_bytes())
                    .unwrap();
            }
        }
    });
}

fn ls_query_files() -> Vec<String>
{
    String::from_utf8(Command::new("ls").arg(QUERIES_DIR).output().unwrap().stdout)
        .unwrap()
        .split_ascii_whitespace()
        .map(|file| format!("{}{}", QUERIES_DIR, file))
        .collect()
}

fn run_predictions(training_file: &str, binary_path: &str) -> Vec<String>
{
    ls_query_files()
        .into_par_iter()
        .enumerate()
        .map(|(i, query_file)| {
            let mut child = Command::new(binary_path)
                .env("RUST_BACKTRACE", "1")
                .arg(&training_file)
                .stdin(Stdio::piped())
                .stdout(Stdio::piped())
                .spawn()
                .expect("failed to spawn process");

            let mut file = File::open(&query_file).expect("failed to open file");
            io::copy(&mut file, child.stdin.as_mut().unwrap()).expect("Can't copy from FILE");
            let output = child.wait_with_output().unwrap();

            println!("Finished nr. {}: {}", i, query_file);

            String::from_utf8(output.stdout).unwrap()
        })
        .collect()
}

fn save_results(results: impl IntoIterator<Item = String>, queries_file_name: &str)
{
    use std::io::prelude::*;

    let file_name = &format!("./results{}", queries_file_name.split('_').skip(1).fold(String::new(), |acc, part| acc + "_" + part));

    let mut file = File::create(file_name).unwrap();
    results
        .into_iter()
        .for_each(|result| file.write_all(result.as_bytes()).unwrap());
}

fn main()
{
    let training_file = args().nth(1).expect("No training data filename provided.");
    let queries_file = args().nth(2).expect("No queries filename provided.");
    let binary_path = args().nth(3).expect("No bin path provided.");

    check_dirs();

    create_query_files(&queries_file);
    save_results(run_predictions(&training_file, &binary_path), &queries_file);
}
