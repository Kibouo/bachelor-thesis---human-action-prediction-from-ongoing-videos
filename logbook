    Date   | Hours |   What?
------------------------------------------------------------------------
 18/02/'19 |     2 | Search/read papers about the subject.
 19/02/'19 |     6 | Search/read papers about the subject.
 21/02/'19 |     1 | First meeting & uploading notes to git
 23/02/'19 |   4.5 | Setup of Latex document & compiler. Research the term `features' and write short description of current understanding.
 26/02/'19 |     5 | Better understand features, read about bag-of-words, read about Bayesian statistics. Write short pieces of text about basic understanding of each of them 1in paper.
 27/02/'19 |     5 | Write about local features specifically. Try to understand which features are being detected and how. Write a short piece about feature gathering specific to the paper.
 28/02/'19 |     2 | Search for code of feature detectors which work with videos.
 01/03/'19 |   4.5 | Read about STIP (spatio-temporal interest point) feature detectors. Search for STIP detector library/software. Test/get it to work: Laptev's STIP detector software.
 05/03/'19 |     7 | Search for data set. Research and write about K-means clustering. Read, understand, and write about human action prediction using integral bag-of-words approach. Try to understand the maths of formula 4 & 5 in the paper.
 06/03/'19 |     1 | Read paragraph Dynamic bag-of-words in paper.
 07/03/'19 |   0.5 | Update vergadering & notities maken.
 08/03/'19 |     2 | Trying to figure out what it means to have normal distributed histograms and how to use them in a pdf (paragraph above formula 4 in paper).
 12/03/'19 |     3 | Write about dynamic bag-of-words in own words. Read dynamic algorithm of paper.
 14/03/'19 |   2.5 | Read & write about dynamic programming. Read about maximum a posteriori estimation. Write about the dynamic programming algorithm used in the paper.
 16/03/'19 |   5.5 | Read about multivariate normal distribution. Read about HOG descriptors and HOF descriptors. Read paper "On Space-Time Interest Points" which describes how they process the found STIPs for action recognition.
 18/03/'19 |   3.5 | Read about HOG descriptors. Try to understand how to perform k-means clustering on HOG descriptors.
 19/03/'19 |     5 | Setup code part of project. Write parser for STIP data.
 21/03/'19 |   1.5 | Meeting and taking notes. Extend STIP parser to dump HOG/HOF data into separate histograms.
 25/03/'19 |    10 | Research OpenCV wrappers in Rust (prototype different wrappers). Try understanding OpenCV API (create Mat from external data). Figure out linking errors with OpenCV (cv-rs) wrapper.
 26/03/'19 |   5.5 | Figure out how to structure Mat wrappers to allow for easy and safe code which works with matrices.
 27/03/'19 |   9.5 | After figuring out the K-means API and calculating the cluster centers, I realised I need to label my features as well. The OpenCV wrapper does not support this so I TRIED to figure out how to make Rust interact with the C++ OpenCV libary and extended the wrapper. However, it kept segfaulting. In the end I decided to simply implement simple things such as covariance and distance calculation myself.
 28/03/'19 |     3 | Looked back at extending the wrapper and it suddenly just worked^TM. (Fork deleted as no modifications were needed).
 29/03/'19 |    11 | Finally figured out how to work with the rust wrapper propperly. Let's get to work on the actual paper now... Also figured out how BFMatcher works (to allow labelling of features onto cluster centers). Carefully re-read chapter on integral histograms and start modelling data structers for it.
 01/04/'19 |   8.5 | Write code for calculating avg integral BOW histograms. Trying to understand how P(t|d) (i.e. similarity between progress, counted in amt of frames) is calculated.
 02/04/'19 |   2.5 | Try finding resources on how to calculate P(t|d) (i.e. the similarity between progresses of videos in amt of frames).
 03/04/'19 |   0.5 | Update meeting + vraag hoe P(t|d) zou kunnen uitgerekend worden. Nota's vergadering schrijven.
 04/04/'19 |   3.5 | Zoeken naar mogelijke manieren om P(t|d) te berekenen (aka similariteit tussen voortgangen van videos).
 05/04/'19 |   7.5 | Write code for query input (stdin) handling, reorganise/clean code (helper fn, methods), and start writing actual prediction function (algo 4 in paper).
 06/04/'19 |     8 | Finish writing out formula 4 in code. Start writing formula 5 (multivariate normal distr.); freshen up knowledge about multi norm distr by reading about it again. Figure out how to interleave calculating covar matrices nicely and efficiently in the code, and start writing the actual code.
 07/04/'19 |     4 | Debug, clean up, and re-arrange code for calculating and passing covar matrices for prediction fn.
 08/04/'19 |   6.5 | Meeting about P(t|d) + taking notes. Refactor covar calculation and help functions. Add some methods to Mat and type out formula 5 (buggy, but bug found).
 09/04/'19 |   1.5 | Look for bug in code of formula 5.
 10/04/'19 |     5 | Look for bug surrounding det(covar_mat) being 0. This was caused by a lack of data -> find and process (write small process script) for a dataset. det(covar_mat) was still 0 at the start of video (logical as we have almost no features detected) -> research the why and how to work around this.
 14/04/'19 |     2 | Generate large amounts of training data for 2 actions. Add a bit of caching to prediction functions.
 23/04/'19 |     5 | Add progress similarity (P(t|d)) calculation and fix caching.
 24/04/'19 |   5.5 | Try to fix multivar norm distr's pdf for big covar matrices (100x100+). The determinant of this covar overflows and the MVN returns NaN.
 25/04/'19 |     8 | Replace ``determinant == 0`` as singularity check with rank calculation. Add comment to singularity check and to log_determinant calculation. Do some benchmarking as training/querying with a big amount of words/cluster centers is too slow.
 26/04/'19 |     4 | Benchmark some more -> kmeans is very slow -> tweak params. Remove dead code. Re-read dynamic programming and think about how to implement it.
 28/04/'19 |   4.5 | Start writing code for dynamic programming part of paper. Research whether substraction of covariance matrices is appropriate/allowed.
 29/04/'19 |     6 | Figure out how to take the difference of 2 auto-covariance matrices. Search for a way to calculate the cross-covariance in OpenCV.
 30/04/'19 |   6.5 | Search for a way to calculate the cross-covariance in OpenCV. Write own cross-covar matrix calculator and test it extensively.
 01/05/'19 |   8.5 | Restructure code to allow for storage of samples. This allows for cross-covar matrix calculation later on. Write (most of) the dynamic function which calculates the action probability. Search for how to & libraries that allow integration over multivariate normal distributions.
 02/05/'19 |  10.5 | Short search for alternatives for calculating the integral of mulitvariate norm distrs. Test found R library. Figure out R dependencies. Figure out how to call R code from within Rust.
 03/05/'19 |     6 | Implement probability calculation for multivariate norm distr using 'mvtnorm' R library. Clean up unused code. Add stop condition for recursion of dynamic similarity calculation and start adding caching.
 04/05/'19 |   1.5 | Add documentatin to dynamic prediction algo function.
 05/05/'19 |     4 | Add caching to dynamic algo. Fix indexing error. Fix feature hist interval covar calculation.
 06/05/'19 |     4 | Performance analysis: mvtnorm R library is taking 60% of all time spent -> can't find any alternative... Trying to find as to why the prediction is always "walking"...
 07/05/'19 |     3 | Remove unneeded code. Fix bug that gave non-deterministic results between runs (k-means didn't run enough). Fix bug that gave too much credit to early observations.
 09/05/'19 |   0.5 | Meeting and taking notes.
 15/05/'19 |    11 | Make a title page for the paper. Properly write out everything to know about features.
 16/05/'19 |    10 | Nicely write out posterior probability calculation and multivariate normal distributions.
 17/05/'19 |   9.5 | Explain HOG and K-means in paper. Also proofread paper.
 18/05/'19 |   9.5 | Explain BoW and write out workings of the paper properly.
 19/05/'19 |  10.5 | Write everything about decisions made and implementation details in paper.
 20/05/'19 |    13 | Write evaluation and create poster.
 24/05/'19 |   4.5 | Fix some typos, write acknowledgement in paper. Prepare code for batch accuracy testing.
 25/05/'19 |   6.5 | Write parallelised testing script.
 26/05/'19 |     6 | Tweak kmeans params. Start writing scriptie paper.
 27/05/'19 |     6 | Tweak kmeans params. Fix really slow invert matrix function. Add a few debug statements as some tests crash. Write script for skipping every second frame. Write parts of scriptie paper.
 28/05/'19 |     5 | Finish writing scriptie.
 30/05/'19 |     5 | Write more about experimenting and results in paper.
 31/05/'19 |     6 | Small fixes in poster and paper. Write dutch resume of the paper. Prepare meeting notes and everything else to be handed in.
 02/06/'19 |     4 | Add experiment results for integral BOW. Rewrite matrix structs to use Rust traits instead of dereferencing).
 07/06/'19 |   7.5 | Reread everything once more for consistency and typos.

