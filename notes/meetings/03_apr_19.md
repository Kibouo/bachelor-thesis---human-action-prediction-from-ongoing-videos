# Nota's vergadering

`03 april 2019`

`EDM 1.05a`

- Vraag over hoe P(t|d) zou kunnen geïmplementeerd worden; check referenties, variaties van de paper, context (journals) waar de paper in voor kwam. Misschien is dit een soort distance calculatie?
- Algemene update: OpenCV wrapper en API uit moeten zoeken. Verder ben ik bijna klaar met implementeren. Enkel formula 3 moet nog en de likelihood functie moet vervangen worden met de recursieve versie.