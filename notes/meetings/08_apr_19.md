# Nota's vergadering

`08 april 2019`

`EDM 1.05a`

- Antwoord op vraag over P(t|d):
    * Dit is een soort compensatie voor uitliggers. Denk aan observaties die na 1 frame al zeggen dat er actie A wordt uitgevoerd, maar gemiddeld is actie A 40 frames lang.
    * Dit is eigenlijk P(t|Ap, d), maar aangezien d en Ap gerelateerd zijn aan elkaar (d is nl. voortgang van Ap), kan Ap weg gelaten worden.
- Similariteit van acties berekenen is de hoofdzaak, P(t|d) is een 'verbetering'. Zeker niet triviaal, maar er kan aan getweaked worden.
- Een mogelijke distributie voor P(t|d) kan Gaussian distr zijn, met gemiddeld en std afwijking gebaseerd op de lengtes van de training videos (voor een bepaalde actie).