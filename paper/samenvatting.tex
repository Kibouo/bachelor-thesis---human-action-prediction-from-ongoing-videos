\chapter{Nederlandse samenvatting}

\section{Introductie}
Het doel van ons onderzoek is het voorspellen van menselijke acties. Dit houdt in dat we menselijke acties gaan classificeren voordat ze compleet uitgevoerd zijn. Dit staat in contrast met het herkennen van menselijke acties. Herkenning is namelijk classificatie van afgeronde observaties.

Deze technologie lijkt ons handig in het voorkomen van diefstallen en moorden. Een computer systeem kan namelijk constant de acties van mensen monitoren. Wanneer er dan een verdachte beweging herkend wordt kan het systeem op tijd alarm slaan.

\subsection{Probleemstelling}
\label{probleem}
Actie herkenning kan gedefinieerd worden als het vinden van de actie, uit alle gekende acties, die meest waarschijnlijk is uitgevoerd geweest, gegeven een observatie. Merk op dat dit een Bayesiaanse gevolgtrekking is. We noteren de mogelijke actie en de lengte hiervan alsook de observatie en deze zijn lengte als $(A_p,d^{*})$ en $(O,t^{*})$ respectievelijk. De formule om te classificeren kan dan genoteerd worden als volgt \cite{ryoo_2011}:

\begin{equation}
\begin{split}
   A^{*}
   &= \argmax_p P(A_p, d^{*} | O, t^{*}) \\
   &= \argmax_p \frac{P(O | A_p, d^{*}) P(t^{*} | d^{*}) P(A_p, d^{*})}{\sum_i P(O | A_i, d^{*}) P(t^{*} | d^{*}) P(A_i, d^{*})}
\end{split}
\end{equation}
$P(t^{*} | d^{*})$ is de gelijkenis tussen de lengte van de mogelijke actie en de lengte van de observatie. Dit wordt gebruikt als compensatie voor observaties met ongewone lengtes. Stel dat een mogelijke actie compleet overeenkomt met de observatie. De observatie is echter 120 seconden lang terwijl de actie maar 5 seconden lang is. Dit groot verschil kan een aanwijzing zijn dat de observatie toch niet de gegeven actie bevat.

Het voorspellen van acties gelijkt op het bovenstaande. Het enige verschil is dat de actie in de observatie nog niet is afgerond. De mogelijke actie waarmee vergeleken wordt moet dan ook eenzelfde voortgangsniveau als de observatie hebben. Een probleem hierbij is echter dat het voortgangsniveau van de actie in de observatie zelf niet gekend is. Daarom wordt de observatie vergeleken met alle voortgangsniveau van een mogelijke actie. Deze aanpassingen resulteren in volgende formule \cite{ryoo_2011}:
    
\begin{equation}
\begin{split}
    A^{*}
    &= \argmax_p \sum_d P(A_p, d | O, t) \\
    &= \argmax_p \frac{\sum_d P(O | A_p, d) P(t | d) P(A_p, d)}{\sum_i \sum_d P(O | A_i, d) P(t | d) P(A_i, d)} \label{eq_a14}
\end{split}
\end{equation}
Waarbij $0 \leq d \leq d^{*}$.

\section{Onderzoek}
De formules besproken in Sectie \ref{probleem} berusten op het berekenen van de gelijkenis tussen acties. Om dit te kunnen doen onderzoeken we eerst wiskundige representaties voor acties. Vervolgens onderzoeken wij twee technieken om de gelijkenis, $P(O | A_p, d)$ in Formule \eqref{eq_a14}, tussen acties te berekenen. De onderzochte technieken komen uit de paper \citetitle{ryoo_2011} \cite{ryoo_2011}.

\subsection{Acties representeren}
Uit een video van een actie worden er eerst kenmerkende eigenschappen onttrokken. Deze eigenschappen, in het Engels features genaamd, zijn kenmerkend in ruimte zowel tijd. Vervolgens worden de features gegroepeerd in verschillende clusters. Dit clusteren doen wij met het $K$-means algoritme \cite{ryoo_2011}.

\clearpage

Per frame wordt er vervolgens een \textit{feature histogram} berekend. Dit is een histogram die per cluster telt hoeveel features, toebehoorende tot die cluster, er tot op dat moment zijn verschenen \cite{ryoo_2011}.
    
Deze histogrammen worden dan chronologisch achtereen geplaatst. Dit vormt een \textit{integral histogram}. Deze histogram kan men zien als een wiskundige representatie van een actie. Het is een functie die beschrijft hoe het aantal features, per cluster, evolueert over tijd \cite{ryoo_2011}.

\subsection{Integral bag of words}
\label{ibow}
\textit{Integral bag of words} is een techniek om de gelijkenis tussen acties te berekenen. De techniek vergelijkt de integrale histogrammen van acties. Acties worden veronderstelt om onderling normaal verdeeld te zijn. Om de gelijkenis op een specifiek voortgangsniveau te weten worden de feature histogrammen van op dat voortgangsniveau vergeleken. Gegeven $h_d(O)$ en $h_d(A)$ de feature histogrammen van de observatie en de mogelijke actie op moment $d$ respectievelijk, kan de gelijkenis functie geschreven worden als volgt \cite{ryoo_2011}:

\begin{equation}
\begin{split}
    P(O | A, d)
    &= M(h_d(O), h_d(A)) \\
    &= \frac{1}{\sqrt{2\pi\sigma^{2}}}e^{\frac{-(h_d(O)-h_d(A))^{2}}{2\sigma^{2}}} \label{eq_a6}
\end{split}
\end{equation}
We gebruiken hier een multivariate normale verdeling aangezien de stochastische variabelen, dit zijn de histogrammen, meerdere dimensies hebben. 

\subsection{Dynamic bag of words}
Merk op dat de techniek besproken in Subsectie \ref{ibow} enkel de hoeveelheid aan features vergelijkt. Het houdt geen rekening met wanneer dat ze verschijnen. De evolutie van een actie doorheen de tijd wordt dus compleet genegeerd. Deze volgorde is echter zeker van belang. De mogelijkheid bestaat namelijk dat twee verschillende acties evenveel features per cluster cre\"eren terwijl ze er op een andere manier aan geraakt zijn \cite{ryoo_2011}.

Om deze temporale ordening te behouden berekent de \textit{dynamic bag of words} techniek de gelijkenis iteratief. Voor deze iteratieve berekening worden de observatie en mogelijke actie eerst verdeeld in intervallen. De features die in deze intervallen verschijnen vormen feature histogrammen. Tussen deze wordt de gelijkenis berekend. Vervolgens worden feature histogrammen voor het volgende paar intervallen berekend. De gelijkenis berekening houdt deze keer echter rekening met de gelijkenis van de vorige stap. Dit iteratief proces wordt herhaald totdat het huidige voortgangsniveau bereikt is \cite{ryoo_2011}.

De grootte van de intervallen is niet statisch. Het doel is om de groottes zo te kiezen dat de gelijkenis tussen elk paar intervallen maximaal is. Dit resulteert namelijk in de maximale gelijkenis tussen de observatie en mogelijke actie. De formule om de gelijkenis te berekenen noteren we als volgt:

\begin{equation}
    P(O^{t} | A, d) = \argmax_{\Delta t, \Delta d} P(O^{t - \Delta t} | A, d - \Delta d) M(h_{\Delta t}(O), h_{\Delta d}(A))
\end{equation}
Waarbij $M$ komt uit Formule \eqref{eq_a6}. $\Delta t$ en $\Delta d$ zijn de groottes van de intervallen voor de observatie en de mogelijke actie respectievelijk \cite{ryoo_2011}.

Om de tijdscomplexiteit van de berekening te verlagen maken wij echter $\Delta t$ toch statisch \cite{ryoo_2011}.

\section{Evaluatie}
Het gebrek van de \textit{integral bag of words} techniek, namelijk het verwaarlozen van de volgorde waarin features verschijnen, heeft een negatieve invloed op de accuraatheid van de classificatie. De techniek is geschikt als tussenstap voor het uitleggen van de \textit{dynamic bag of words} techniek. Het is echter niet aangeraden om het te gebruiken bij het voorspellen van menselijke acties.

De \textit{dynamic bag of words} techniek is echter zeer geschikt voor het voorspellen. Het is zeer accuraat gegeven een groot genoeg aantal clusters. Ook deze techniek heeft echter nog een tekortkoming. Het verwaarloost namelijk de ruimtelijke informatie van features. Wij vermoeden dat het in acht nemen van deze informatie een nog accurater algoritme als gevolg zou kunnen hebben.

Onze implementatie werkt helaas niet in realtime. De grootste hindernissen zijn het onttrekken van features en de groepering hiervan. De tijdscomplexiteit van de groepering en de accuraatheid van de classificatie zijn omgekeerd gerelateerd. Dit maakt het moeilijk om een juiste balans hiertussen te vinden.

We merken echter op dat beide componenten vervangbaar zijn. Dit kan mogelijk in een snellere en accuratere implementatie resulteren.